# راهنمای استفاده از گیت لب

راهنمای استفاده از گیت برای اطلاع رسانی  و ارتباط با بخش فنی موسسه دانش پژوهان

طبق سیاست های کاری بخش فنی و زیرساختی موسسه دانش پژوهان برای ارتباط سازمانی با بخش های مختلف این دپارتمان باید از طریق وب سایت [Gitlab](Gitlab.com) .اقدام به ثبت نام و ارسال پیام ها و فایل های خود نمایید

# مراحل ثبت نام و ارائه را در زیر دنبال کنید :

1.  ابتدا برای ثبت نام به وب سایت [GitLab](Gitlab.com) مراجعه نمایید.


2.  سپس بر روی دکمه Register کلیک می کنید.


![1](/images/1.png)

3.  در اینجا چند راه ورود دارید، یکی ثبت نام داخل سایت که با پر کردن فیلد های زیر امکان پذیر می باشد و یا ورود از طریق Google که راحت تر می باشد.

![2](/images/2.png)

4.  بعد از ثبت نام و ورود برای اولین بار به گیت لب به صفحه زیر وارد خواهید شد.

>  دقت فرمایید که اگر از طریق فرم ثبت نام کرده باشید باید ایمیل خود را چک کنید و اکانت خود را تایید کنید تا بتوانید وارد پنل کابری خود شوید.


![3](/images/3.png)

5.   برای ارتباط و اضافه شدن به گروه Dpsoft باید ایمیل و نام کاربری ثبت نامی خود در این سایت ، موضوع و یا نام پروژه ای که میخواهید در رابطه با آن ارتباط برقرار شود را به ایمیل زیر برای بخش فنی ارسال نمایید.

>  Email DpSoft : dpsoft@dpsoft.ir
 
6.  پس از تایید شدن و افزودن شما به پروژه مربوطه  ایمیلی برای تایید ارسال خواهد شد و میتوانید با وارد شدن به پنل کاربری خود با صفحه زیر مواجه می شوید.

![4](/images/4.png)

7.  همانطور که در بالا ملاحظه می کنید می فرمایید لیستی از پروژه هایی که شما در آن ها اضافه شده اید را مشاهده می کنید و میتوانید به آن ها وارد شده و طبق مراحل زیر پیام خود را برای بخش فنی ارسال نمایید.




#  مراحل ارسال ISSUE در گیت لب

شما می توانید برای ارسال پیام ها ، فایل ها ، درخواست ها و ... از طریق سیستم ایشو که در هر پروژه وجود دارد با بخش فنی این موارد را در میان بگذارید.
برای ارسال ایشو نیاز به ثبت نام ، ورود و اضافه شدن شما به پروژه مربوطه می باشد.

>  دقت فرمایید در هر ایشو درخواست مربوط به هر پروژه را ارسال نمایید و الا به درخواست شما پاسخ داده نخواهد شد!

# لطفا برای ارسال ایشو مراحل زیر را دانبال نمایید :

1.  ابتدا وارد پنل گیت لب خود شوید و در داشبورد خود مانند تصویر زیر لیست پروژه های مربوطه را مشاهده می فرمایید.

![4](/images/4.png)

2.  حال با انتخاب پروژه ای که در رابطه با ان درخواستی دارید کلیک می کنید و وارد صفحه مربوطه می شوید.

![5](/images/5.png)


3.  حال از منوی سمت راست گزینه Issues را انتخاب می کنیم.

![5](/images/5.png)


4.  حال در صفحه وارد شده برو روی دکمه New issue کلیک می کنیم

![6](/images/6.png)

5.  حال وارد صفحه درخواست ایشو شده ایم و می توانید توضیحات مربوط به هر قسمت را در عکس های زیر مشاهده فرمایید.

![7](/images/7.png)

![8](/images/8.png)

6. پس از تکمیل درخواست قسمت های مربوطه بازدن گزینه Submit issue درخواست خود را برای بخش فنی ارجاع دهید و منتظر پاسخ از بخش فنی بمانید.


# شرایط و ضوابط برای ارسال ایشو

قواعدی برای رسیدگی آسان تر به درخواست ها تدوین شده است که می تواند به سریعتر رسیدگی شدن ایشو شما منجر شود.

1.  لطفا درخواست خود را در پروژه مربوطه ارسال نمایید و گرنه به درخواست شما رسیدگی نخواهد شد.
2.  برای هر موضوع با بحث جداگانه لطفا ایشو های  جدید ارسال نمایید
3.  هر ایشو را تا حل شدن و خاتمه یافتن موضوع ادامه دهید و از باز کردن ایشو جدید خودراری فرمایید.
4.   برای ارسال کد های کوتاه از قسمت `insert code` در بخش ارسال ایشو کمک بگیرید و برای کد های طولانی از فایل `TXT `استفاده کنید
5.   پاسخ درخواست هم از طریق ایمیل و هم در صفحه ارسال درخواست قابل مشاهده می باشد .
6.   پس از ارسال درخواست و دریافت پاسخ در صورتی که مایل به ارسال پاسخ مجدد هستید از ارسال تیکت جدید خودداری نموده و در همان محاوره درخواست خود را ارائه دهید.
7.    در نظر داشته باشید بخش فنی در جریان مشکل و موقعیت شما نیست و شما می بایست مشکل  و یا  موقعیت خود را به صورت کامل و واضح مطرح نمایید(و در صورت لزوم با ارسال تصویر و ...مشکل خود را شفاف نمایید).
8.    در صورتی که درخواست شما در وضعیت "درحال بررسی" و یا به فرد خاصی ارجاع شده است ، می بایست منتظر دریافت نتیجه و پاسخ از همان طریق بمانید و از ارسال پاسخ جهت پیگیری مجدد جدا خودداری نمایید .
9.    در انتخاب بخش مورد نظر جهت ارسال درخواست دقت لازم را داشته باشید ، در صورتی که بخش مربوطه صحیح نباشد بررسی و پیگیری درخواست شما به تعلیق خواهد افتاد.
10.   هر ایشو همانند یک نامه رسمی می باشد ، بنابراین از ادبیات مناسب استفاده و از بکاربردن تعداد غیر معمول علامت سوال (؟؟؟) و یا (!!!!) ، الفاظ عامیانه ، تهدید ، توهین ، شوخی و ... خودداری نمایید چرا که متن شما را از حالت رسمی خارج و تاثیر منفی در پی خواهد داشت .
11.    در صورتی که تیکت شما در وضعیت "Close" است یعنی نیاز به ادامه گفتگو نیست و یا شما موارد فوق را رعایت ننموده اید.
12.   لطفا ایشو های خود را که پیگیری آن به پایان رسیده و یا مشکل آن حل شده است را با زدن Close issue آن را بسته و از پاسخ مجدد به ایشو های بسته خود داری فرمایید لذا جواب درخواست شما با مشکل مواجه خواهد شد.
13.   استفاده از عنوان و محتوای مناسب برای ارسال ایشو مسلما در کیفیت و سرعت پاسخگویی تاثیرگذار خواهد بود، لذا سعی نمایید درخواست خود را با جزییات فنی و کاملا دقیق بیان فرمایید.
14.   در صورت مشاهده خطا و یا مشکلات مرتبط با سایت و دریافت ارور و موارد مشابه نیز پیشنهاد می گردد متن ارور و یا اسکرین شات آن را در قسمت ضمیمه ها به ایشو خود اضافه نمایید.

# شکن

با شکن دیگر نگران تحریم های اینترنتی نیستیم چون یک تحریم شکن هست که ما رو  از دست وی پی ان ها نجات میده و میتونیم بدون دغدغه به سایت ها مفیدی که ما رو فیلتر کردند وارد بشیم بدون در دسر

# راهنمای تنظیمات شکن برای ویندوز

روی منوی Start کلیک کنید و سپس روی کنترل پنل (Control Panel) کلیک کنید.

روی گزینه Network and Internet کلیک کنید.

روی گزینه Change Adapter Settings کلیک کنید.

روی شبکه وای‌فای یا شبکه LAN که به آن متصل هستید راست کلیک کنید و سپس Properties را انتخاب کنید.

گزینه Internet Protocol Version 4 را انتخاب کنید.

دکمه Properties را بزنید.

اگر نشانی DNS وجود دارد آن را جایی یادداشت کنید و برای مراجعات بعدی نگه دارید. سپس آنها را پاک کنید.

نشانی 178.22.122.100 و 94.232.174.194 را در بخش‌های مربوط به DNS وارد کنید.

دکمه OK را بزنید و پنجره را ببندید.

مرورگر خود را ری‌استارت نمایید.

